use frankenstein::{GetUpdatesParams, MessageEntity, MessageEntityType, User};
use frankenstein::SendMessageParams;
use frankenstein::TelegramApi;
use frankenstein::{Api, UpdateContent};



fn main() {
    let api_token: &str = &*std::env::var("TELEGRAM_API_TOKEN")?;
    let api = Api::new(api_token);

    let update_params_builder = GetUpdatesParams::builder();
    let mut update_params = update_params_builder.clone().build();

    let own_user_id = std::env::var("OWN_USER_ID")?;
    let own_user_first_name = std::env::var("OWN_USER_FIRST_NAME")?;
    let own_user_chat_id = std::env::var("OWN_USER_CHAT_ID")?;

    let own_user = User::builder()
        .id(own_user_id.parse::<u64>().unwrap())
        .first_name(own_user_first_name)
        .is_bot(false)
        .build();
    loop {
        let result = api.get_updates(&update_params);

        match result {
            Ok(response) => {
                for update in response.result {
                    match update.content {
                        UpdateContent::Message(message) => {
                            println!("From chat: {:?}", message.chat);
                            let new_chat_member = message.new_chat_members;
                            match new_chat_member {
                                Some(user) =>  {
                                    let message_string = format!("Hoi {}! {} zal zo contact met je opnemen, even geduld :)", user[0].first_name, own_user.first_name);
                                    let mention_offset: u16 = message_string.find("!").unwrap() as u16;
                                    let username_length = own_user.first_name.len() as u16;

                                    let user_mention = MessageEntity::builder()
                                        .type_field(MessageEntityType::TextMention)
                                        .offset(mention_offset + username_length - 1)
                                        .length(username_length)
                                        .user(own_user.clone())
                                        .build();

                                    let join_reply = SendMessageParams::builder()
                                            .chat_id(message.chat.id)
                                            .text(message_string.clone())
                                            .reply_to_message_id(message.message_id)
                                            .entities( vec![user_mention.clone()])
                                            .build();
                                    let private_reply = SendMessageParams::builder()
                                            .chat_id(own_user_chat_id.parse::<u64>().unwrap())
                                            .text(format!("A user has joined the group: {:?}", user[0]))
                                            .build();

                                    if let Err(err) = api.send_message(&join_reply) {
                                                println!("Failed to send message: {err:?}");
                                            }
                                    if let Err(err) = api.send_message(&private_reply) {
                                                println!("Failed to send message: {err:?}");
                                            }
                                }
                                None => {println!("Not a new user")}
                            }
                        }
                        _ => {
                            println!("Not a join event, ignoring message");
                        }
                    }
                    update_params = update_params_builder
                        .clone()
                        .offset(update.update_id + 1)
                        .build();
                }
            }
            Err(error) => {
                println!("Failed to get updates: {error:?}");
            }
        }
    }
}
